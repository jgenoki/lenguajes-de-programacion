(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)

    
fun all_except_option (str: string , lst: string list) =
  if null lst 
  then NONE
  
    ELSE all_except_option (str, lst) =
    case same_string(hd lst, str) of
      true  => SOME lst
    | false => case all_except_option(str,tl lst) of
                 NONE   => NONE
               | SOME y => SOME (lst::y);
  
  

val str= "jenedith";
val mlist= ["jorguito" , "randyzito" , "jeneditah" , "jenedith" ];

fun main n = 
  all_except_option ( str, mlist) ;
main 0;
