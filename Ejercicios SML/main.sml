(*ejercicio 1*)

fun is_older(date1 : int*int*int , date2 : int*int*int) =
  if (#3 date1) - (#3 date2) = 0
  then 
    if (#2 date1) - (#2 date2) = 0
    then
      if (#1 date1) - (#1 date2) < 0
      then true
      else false
    else 
      if (#2 date1) - (#2 date2) < 0
      then true
      else false
  else
    if (#3 date1) - (#3 date2) < 0
    then true
    else false ;

(*ejercicio 2*)

fun number_in_month(date : (int*int*int)list , month : int) =
  if null date
  then 0
  else 
    if (#2(hd date)) = month then 1 + number_in_month(tl date , month)
    else number_in_month(tl date , month);
    
(*ejercicio 3*)

fun numbers_in_months(dates : (int*int*int)list , month : int list) =
  if null dates
  then []
  else
    if (#2(hd dates)) = (hd month) 
    then hd dates :: numbers_in_months(tl dates , tl month)
    else numbers_in_months(tl dates , tl month);
    
(*ejercicio 4*)  

fun dates_in_month(dates : (int*int*int)list , month : int) =
  if null dates
  then []
  else
    if (#2(hd dates)) = month
    then hd dates :: dates_in_month(tl dates , month)
    else dates_in_month(tl dates , month);

(*ejercicio 5*)

fun dates_in_months(dates : (int*int*int)list , month : int list) =
  if null dates
  then []
  else dates_in_month(dates , hd month) @ dates_in_months(dates , tl month);

(*ejercicio 6*)

fun get_nth (palabras : string list, n : int) =
    if n = 0
    then "No se puede encontrar ese elemento"
    else 
      if n = 1
      then hd palabras 
      else get_nth (tl palabras, n - 1);

val meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octobre", "Noviembre", "Diciembre"];

(*ejercicio 7*)

fun date_to_string (date : (int * int * int)) =
  if date = (0,0,0)
  then "No hay fecha"
  else Int.toString (#1 date) ^ " " ^ get_nth (meses, #2 date) ^ " " ^ Int.toString(#3 date);
  

(*ejercicio 8*)

fun number_before_reaching_sum (suma : int, valores : int list) =
    let
        fun iterate_sum (i : int, parar : int, max : int, vec : int list) =
            if parar + hd vec >= max
            then i - 1
            else iterate_sum (i + 1, parar + hd vec, max, tl vec)
    in 
        iterate_sum (1, 0, suma, valores)
    end;
    
(*ejercicio 9*)

val meses = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
fun what_month (dia : int) =
  number_before_reaching_sum (dia, meses) + 1;


(*ejercicio 10*)

fun month_range (dia1 : int, dia2 : int) =
    if dia1 > dia2
    then []
    else 
        let fun contar (desde : int, hasta : int) =
            if desde = hasta
            then [hasta]
            else desde :: contar (desde + 1, hasta)
        in
            contar (what_month (dia1), what_month (dia2))
        end


(*ejercicio 11*)

fun oldest (dates : (int * int * int) list) =
    if null dates
        then NONE
    else
        let fun get_oldest (dates : (int * int * int) list) =
                if null (tl dates)
                then hd dates
                else
                    let
                        val ultimo = get_oldest (tl dates)
                        val primero = hd dates
                    in
                        if is_older (primero, ultimo)
                        then primero
                        else ultimo
                    end
        in
            SOME (get_oldest dates)
        end
(*Main para pruebas*)
val fecha = (12,3,1990);
val list_int = [1,2,3,4,5,6,7,8,9];
fun main m =
  what_month(3);
  
main 0;